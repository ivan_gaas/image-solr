FROM wodby/base:0.1
MAINTAINER Wodby <hello@wodby.com>

RUN export JDK_VERSION=7 && \
    export SOLR_VERSION=5.3.0 && \
    apk add --update openjdk${JDK_VERSION}-jre-base bash && \
    wget -qO- http://www.us.apache.org/dist/lucene/solr/${SOLR_VERSION}/solr-${SOLR_VERSION}.tgz | tar xz -C /tmp/ && \
    mv /tmp/solr-${SOLR_VERSION} /opt/solr && \
    cd /opt/solr/ && rm -rf docs licenses example && \
    rm -rf /var/cache/apk/* /tmp/*

COPY rootfs /

CMD ["-c5","-t0","/etc/s6"]